function TodoCtrl($scope) {
  $scope.todos = [
    {text:'Learn Angular', done:true},
    {text:'Build an Angular App', done:false}];
 
  $scope.addTodo = function() {
    $scope.todos.push({text:$scope.todoText, done:false});
    $scope.todoText = '';
  };
 
  $scope.remaining = function() {
    var count = 0;
    angular.forEach($scope.todos, function(todo) {
      count += todo.done ? 0 : 1;
    });
    return count;
  };
 
  $scope.archive = function() {
    var oldTodos = $scope.todos;
    $scope.todos = [];
    angular.forEach(oldTodos, function(todo) {
      if (!todo.done){ 
        $scope.todos.push(todo); console.log('inserting: todo.text: '+todo.text); 
      }else{ 
        console.log('removing: todo.text: '+todo.text); 
      } 
    });
  };

  $scope.remove = function($todo){

    var oldTodos = $scope.todos;
    $scope.todos = [];
    angular.forEach(oldTodos, function(todo){
      if(todo != $todo) $scope.todos.push(todo);
    });


  };


}